<?php

namespace App\Controllers;

use App\Models\TArea;
use App\Models\API;
use App\Models\TScan;
use App\Models\TScanUnloading;
use \DateTime;
use \DateTimeZone;

class Data extends BaseController
{
    protected $scanModel;
    protected $scanUnloadingModel;
    protected $areaModel;
    protected $apiModel;
    public function __construct()
    {
        $this->areaModel = new TArea();
        $this->apiModel = new API();
        $this->scanUnloadingModel = new TScanUnloading();
        $this->scanModel = new TScan();
    }

    private function suppriler()
    {
        $date = $this->request->getGet('date') == null ? date('Y/m/d') : $this->request->getGet('date');
        $filterDate = date('Y/m/d', strtotime($date));
        $formattedDatas = []; //array penampung data yang sudah diformat
        $uniquePlatNo = []; //array penampung nama supplier yang sudah unik
        //melakukan iterasi terhadap hasil fetch data dari supplier

        $filteredResults = [];
        $drivers = [];
        $filterTime = [];
        $filterSup = [];
        // dd($this->apiModel->getItem($filterDate));

        foreach ($this->apiModel->getItem($filterDate) as $datTruck) {
            // dd($datTruck);
            foreach ($datTruck['results'][0] as $truck) {
                $platNo = $truck['kendaraan_polisi'];
                //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
                if (!in_array($platNo, $uniquePlatNo)) {
                    $uniquePlatNo[] = $platNo; //tambahkan ke array uniqueSupplierNames
                } else {
                    continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
                }
            }
        }

        foreach ($this->apiModel->getItem($filterDate) as $datSupplier) {
            foreach ($datSupplier['results'][0] as $supplier) {
                $date = $this->request->getGet('date');
                $filterDate = $date != null ? date('d-m-Y', strtotime($date)) : null;
                $past = $filterDate != null ? date('d-m-Y', strtotime('-1 day', strtotime($filterDate))) : date('d-m-Y', strtotime('-1 day'));

                if (date_format(date_create($supplier['confirm_delivery_plan']), 'd-m-Y') == date_format(date_create($supplier['date_delivery_plan']), 'd-m-Y')) {
                    // $url = 'https://portal2.incoe.astra.co.id/vendor_rating/api/dn_json/' . $datSupplier['no_dn'];
                    // $response = file_get_contents($url);
                    // $data = json_decode($response, true);
                    $supplierName = $supplier['nama_supplier'];
                    $limTime = "16-MAR-2023 00:30";
                    $fLimTime = date_create($limTime);
                    $timePlanIn = date_format(date_create($supplier['confirm_delivery_plan']), 'H:i') != '00:00' ? date_create($supplier['confirm_delivery_plan']) : date_create($supplier['date_delivery_plan']);
                    $fTimePlanIn = date_format(date_create($supplier['confirm_delivery_plan']), 'H:i') != '00:00' ? date("H", strtotime($supplier['confirm_delivery_plan'])) * 60 + date("i", strtotime($supplier['confirm_delivery_plan'])) : date("H", strtotime($supplier['date_delivery_plan'])) * 60 + date("i", strtotime($supplier['date_delivery_plan']));
                    $fTimeSecIn = date("H", strtotime($supplier['receipt_security'])) * 60 + date("i", strtotime($supplier['receipt_security']));
                    $fTimeSecOut = date("H", strtotime($supplier['date_done'])) * 60 + date("i", strtotime($supplier['date_done']));
                    // $ftimeScan = date("H", strtotime($timeScan)) * 60 + date("i", strtotime($timeScan));
                    $fTimeQC = date("H", strtotime($supplier['qa_date'])) * 60 + date("i", strtotime($supplier['qa_date']));
                    $fTimeWH = date("H", strtotime($supplier['warehouse_date'])) * 60 + date("i", strtotime($supplier['warehouse_date']));

                    $timeMin = $timePlanIn->getTimestamp() - $fLimTime->getTimestamp();
                    $timeMax = $timePlanIn->getTimestamp() + $fLimTime->getTimestamp();
                    // $total_minutes = date("H", strtotime($timeDiff)) * 60 + date("i", strtotime($timeDiff));
                    $timeMaxFormatted = date_create('@' . $timeMax)->format('H:i');
                    $timeMinFormatted = date_create('@' . $timeMin)->format('H:i');
                    $dateNow = date_format(date_create($supplier['confirm_delivery_plan']), 'd-M-Y');

                    $formattedData = []; //array penampung data supplier yang sudah diformat

                    //melakukan iterasi terhadap hasil fetch data dari TArea
                    foreach ($this->areaModel->getItem() as $areaItem) {
                        // if ($supplierName === $areaItem['name_supplier']) {
                            $status = array_column($this->scanModel->getItem(), 'status');
                            $noDNs = array_column($this->scanModel->getItem(), 'noDN');
                            $key = array_search($supplier['no_dn'], $noDNs);

                            $statusUnloading = array_column($this->scanUnloadingModel->getItem(), 'status');
                            $noDNUnloading = array_column($this->scanUnloadingModel->getItem(), 'noDN');
                            $keyUnloading = array_search($supplier['no_dn'], $noDNUnloading);

                            if ($keyUnloading !== false) {
                                $datasUnloading = $supplier['no_dn']; //tambahkan ke array uniqueSupplierNames
                                $dTimeScanUnloading = $this->scanUnloadingModel->getItem()[$keyUnloading]['created_at'];
                                $timeScanUnloading = (new DateTime($dTimeScanUnloading))->setTimezone(new DateTimeZone('Asia/Jakarta'))->format('H:i');
                                $ftimeScanUnloading = date("H", strtotime($timeScanUnloading)) * 60 + date("i", strtotime($timeScanUnloading));
                            } else {
                                $datasUnloading = "-";
                                $timeScanUnloading = "-";
                                $ftimeScanUnloading = 0;
                            }

                            if ($key !== false) {
                                $datas = $supplier['no_dn']; //tambahkan ke array uniqueSupplierNames
                                $dTimeScanQCR = $this->scanModel->getItem()[$key]['created_at'];
                                $timeScan = (new DateTime($dTimeScanQCR))->setTimezone(new DateTimeZone('Asia/Jakarta'))->format('H:i');
                                $ftimeScan = date("H", strtotime($timeScan)) * 60 + date("i", strtotime($timeScan));
                            } else {
                                $datas = "-";
                                $timeScan = "-";
                                $ftimeScan = 0;
                            }
                            
                            $formattedData = [
                                'tPlanIn' => date_format(date_create($supplier['confirm_delivery_plan']), 'H:i') != '00:00' ? date_format(date_create($supplier['confirm_delivery_plan']), 'H:i') : date_format(date_create($supplier['date_delivery_plan']), 'H:i'),
                                'tSecIn' => date_format(date_create($supplier['receipt_security']), 'H:i'),
                                'tQC' => date_format(date_create($supplier['qa_date']), 'H:i'),
                                'tWHC' => date_format(date_create($supplier['warehouse_date']), 'H:i'),
                                'tSecOut' => date_format(date_create($supplier['date_done']), 'H:i'),
                                'dPlanIn' => date_format(date_create($supplier['confirm_delivery_plan']), 'd-m-Y'),
                                'dSecIn' => date_format(date_create($supplier['receipt_security']), 'd-m-Y'),
                                'dQC' => date_format(date_create($supplier['qa_date']), 'd-m-Y'),
                                'dWHC' => date_format(date_create($supplier['warehouse_date']), 'd-m-Y'),
                                'dSecOut' => date_format(date_create($supplier['date_done']), 'd-m-Y'),
                                'nama_supplier' => $supplierName,
                                'noDN' => $supplier['no_dn'],
                                'priority' => $supplier['URGENT'],
                                'linkItem' => 'https://portal2.incoe.astra.co.id/vendor_rating_infor/api/dn_json/' . $supplier['no_dn'],
                                'area' => $areaItem['area'],
                                'platNo' => $supplier['kendaraan_polisi'],
                                'timeDiff' => $fTimeSecOut - $fTimeSecIn,
                                'timeDiffSecIn' => $fTimeSecIn - $fTimePlanIn,
                                'timeDiffQC' => ((date_format(date_create($supplier['receipt_security']), 'd-m-Y') == '01-01-1980') || (date_format(date_create($supplier['receipt_security']), 'd-m-Y') == $past) ? 0 : $fTimeQC - $fTimeSecIn),
                                'timeDiffWHC' => ((date_format(date_create($supplier['qa_date']), 'd-m-Y') == '01-01-1980') || (date_format(date_create($supplier['qa_date']), 'd-m-Y') == $past) ? 0 : $fTimeWH - $fTimeQC),
                                'timeDiffSecOut' => ((date_format(date_create($supplier['warehouse_date']), 'H:i') == '07:00') || (date_format(date_create($supplier['warehouse_date']), 'd-m-Y') == $past) ? 0 : $fTimeSecOut - $fTimeWH),
                                'nDriver' => $supplier['driver'],
                                'max' => $timeMaxFormatted,
                                'min' => $timeMinFormatted,
                                'statusUnloading' => $statusUnloading,
                                'tScanUnloading' => $timeScanUnloading,
                                'dnScanUnloading' => $datasUnloading,
                                'ftimeScanUnloading' => $ftimeScanUnloading,
                                'status' => $status,
                                'tScanQCR' => $timeScan,
                                'dnScan' => $datas,
                                'ftimeScan' => $ftimeScan,
                                'fTimeQC' => $fTimeQC,
                                'fTimeWHC' => $fTimeWH,
                                'fTimeSecIn' => $fTimeSecIn

                                // 'iQTY' => $fixData['QTY'],
                                // 'iPONO' => $fixData['PONO'],
                                // 'iITEM' => $fixData['ITEM'],
                                // 'iPO' => $fixData['PO'],
                                // 'count' => $count,
                            ];
                            break;
                        // }
                    }


                    if (!empty($formattedData)) {

                        //jika formattedData tidak kosong, maka masukkan data tersebut ke array formattedDatas
                        $scanSecIn = $formattedData['dSecIn'] == '01-01-1970' || $formattedData['dSecIn'] == '01-01-1980' || $formattedData['dSecIn'] == $past;
                        $scanQcRec = $formattedData['noDN'] != $formattedData['dnScan'] || $formattedData['status'] == 'false';
                        $scanQc = $formattedData['dQC'] == '01-01-1980' || $formattedData['dQC'] == $past;
                        $scanUnloadingFinish = $formattedData['noDN'] != $formattedData['dnScanUnloading'] || $formattedData['statusUnloading'] == 'false';
                        $scanWhc = $formattedData['tWHC'] == '07:00' || $formattedData['dWHC'] == $past;
                        $scanSecOut = $formattedData['dSecOut'] == '01-01-1980' || $formattedData['dSecOut'] == $past;

                        $formattedData['onTime'] =
                            $formattedData['tSecIn'] <= $formattedData['max']
                            && $formattedData['tSecIn'] >= $formattedData['min']
                            // && $formattedData['tSecIn'] <= $formattedData['tScanQCR']
                            // && $formattedData['tSecIn'] <= $formattedData['tQC']
                            // && $formattedData['tSecIn'] <= $formattedData['tWHC']
                            // && $formattedData['tSecIn'] <= $formattedData['tScanUnloading']
                            && $formattedData['tSecIn'] <= $formattedData['tSecOut']

                            // && $formattedData['tScanQCR'] >= $formattedData['tSecIn']
                            // && $formattedData['tScanQCR'] <= $formattedData['tQC']
                            // && $formattedData['tScanQCR'] <= $formattedData['tWHC']
                            // && $formattedData['tScanQCR'] <= $formattedData['tScanUnloading']
                            // && $formattedData['tScanQCR'] <= $formattedData['tSecOut']

                            // && $formattedData['tQC'] >= $formattedData['tSecIn']
                            // && $formattedData['tQC'] >= $formattedData['tScanQCR']
                            // && $formattedData['tQC'] <= $formattedData['tWHC']
                            // && $formattedData['tQC'] <= $formattedData['tScanUnloading']
                            && $formattedData['tQC'] <= $formattedData['tSecOut']

                            // && $formattedData['tScanUnloading'] >= $formattedData['tSecIn']
                            // && $formattedData['tScanUnloading'] >= $formattedData['tScanQCR']
                            // && $formattedData['tScanUnloading'] >= $formattedData['tQC']
                            // && $formattedData['tScanUnloading'] <= $formattedData['tWHC']
                            // && $formattedData['tScanUnloading'] <= $formattedData['tSecOut']

                            // && $formattedData['tWHC'] >= $formattedData['tSecIn']
                            // && $formattedData['tWHC'] >= $formattedData['tScanQCR']
                            // && $formattedData['tWHC'] >= $formattedData['tQC']
                            // && $formattedData['tWHC'] >= $formattedData['tScanUnloading']
                            && $formattedData['tWHC'] <= $formattedData['tSecOut']

                            && $formattedData['tSecOut'] >= $formattedData['tSecIn']
                            // && $formattedData['tSecOut'] >= $formattedData['tScanQCR']
                            && $formattedData['tSecOut'] >= $formattedData['tWHC']
                            && $formattedData['tSecOut'] >= $formattedData['tQC'];
                        // && $formattedData['tSecOut'] >= $formattedData['tScanUnloading'];

                        $formattedData['onTimeWH'] =
                            $formattedData['tWHC'] <= $formattedData['tSecOut']
                            && $formattedData['tSecOut'] >= $formattedData['tWHC'];

                        $formattedData['onTimeQC'] =
                            $formattedData['tQC'] <= $formattedData['tSecOut']
                            && $formattedData['tSecOut'] >= $formattedData['tQC'];

                        //JUST SEE THE DATA
                        // $formattedData['notScan'] = [
                        //     'scanSecIn' => $scanSecIn,
                        //     'scanQcRec' => $scanQcRec,
                        //     'scanQc' => $scanQc,
                        //     'scanUnloadingFinish' => $scanUnloadingFinish,
                        //     'scanUnloadingFinish' => $scanUnloadingFinish,
                        //     'scanSecOut' => $scanSecOut
                        // ];

                        $formattedData['notScan'] =
                            ($scanSecIn == true
                                // || $scanQcRec == true
                                || $scanQc == true
                                // || $scanUnloadingFinish == true
                                || $scanWhc == true
                                || $scanSecOut == true)
                            && !($scanSecIn == false
                                // && $scanQcRec == false
                                && $scanQc == false
                                // && $scanUnloadingFinish == false
                                && $scanWhc == false
                                && $scanSecOut == false
                            );

                        $formattedData['outOffSchedule'] =
                            $formattedData['tSecIn'] <= $formattedData['max']
                            && $formattedData['tSecIn'] >= $formattedData['min'];

                        $formattedDatas[] = $formattedData;
                    }
                }
            }
        }

        // dd($formattedDatas);

        foreach ($formattedDatas as $truck) {
            $driver = $truck['platNo'];
            if (!in_array($driver, $drivers)) {
                $filteredResults[] = $truck;
                $drivers[] = $driver;
            }
            $count = count($drivers); //untuk menghitung total truck
        }

        foreach ($formattedDatas as $time) {
            $times = $time['tPlanIn'];
            if (!in_array($times, $filterTime)) {
                $filterTime[] = $times;
            }
        }

        foreach ($formattedDatas as $sup) {
            $sups = $sup['nama_supplier'];
            if (!in_array($sups, $filterSup)) {
                $filterSup[] = $sups;
            }
        }

        $validCountSecIn = array(
            '07:00' => 0, // Variabel counter untuk menghitung jumlah dSecIn jam 07:00
            '08:00' => 0,
            '09:00' => 0,
            '10:00' => 0,
            '11:00' => 0,
            '12:00' => 0,
            '13:00' => 0,
            '14:00' => 0,
            '15:00' => 0,
            '16:00' => 0,
            '17:00' => 0
        );

        $validCountPlanIn = array(
            '07:00' => 0, // Variabel counter untuk menghitung jumlah dSecIn jam 07:00
            '08:00' => 0,
            '09:00' => 0,
            '10:00' => 0,
            '11:00' => 0,
            '12:00' => 0,
            '13:00' => 0,
            '14:00' => 0,
            '15:00' => 0,
            '16:00' => 0,
            '17:00' => 0
        );
        foreach ($filteredResults as $countTruckIn) {
            if ($countTruckIn['dSecIn'] == '01-01-1980' || $countTruckIn['dSecIn'] == $past) {
                continue;
            } else {
                // Filter berdasarkan jam (misalnya, hanya menghitung data pada jam 07:00 dan 08:00)
                $tSecInHour = date('H', strtotime($countTruckIn['tSecIn']));
                if ($tSecInHour == '07') {
                    $validCountSecIn['07:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 07:00 hingga 07:59
                } elseif ($tSecInHour == '08') {
                    $validCountSecIn['08:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
                } elseif ($tSecInHour == '09') {
                    $validCountSecIn['09:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
                } elseif ($tSecInHour == '10') {
                    $validCountSecIn['10:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
                } elseif ($tSecInHour == '11') {
                    $validCountSecIn['11:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
                } elseif ($tSecInHour == '12') {
                    $validCountSecIn['12:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
                } elseif ($tSecInHour == '13') {
                    $validCountSecIn['13:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
                } elseif ($tSecInHour == '14') {
                    $validCountSecIn['14:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
                } elseif ($tSecInHour == '15') {
                    $validCountSecIn['15:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
                } elseif ($tSecInHour == '16') {
                    $validCountSecIn['16:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
                } elseif ($tSecInHour == '17') {
                    $validCountSecIn['17:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
                }
            }
        }

        $hourlyData = [
            '07:00' => $validCountSecIn['07:00'],
            '08:00' => $validCountSecIn['08:00'],
            '09:00' => $validCountSecIn['09:00'],
            '10:00' => $validCountSecIn['10:00'],
            '11:00' => $validCountSecIn['11:00'],
            '12:00' => $validCountSecIn['12:00'],
            '13:00' => $validCountSecIn['13:00'],
            '14:00' => $validCountSecIn['14:00'],
            '15:00' => $validCountSecIn['15:00'],
            '16:00' => $validCountSecIn['16:00'],
            '17:00' => $validCountSecIn['17:00'],
        ];

        foreach ($filteredResults as $countTruckIn) {
            // Filter berdasarkan jam (misalnya, hanya menghitung data pada jam 07:00 dan 08:00)
            $tSecInHour = date('H', strtotime($countTruckIn['tPlanIn']));
            if ($tSecInHour == '07') {
                $validCountPlanIn['07:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 07:00 hingga 07:59
            } elseif ($tSecInHour == '08') {
                $validCountPlanIn['08:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
            } elseif ($tSecInHour == '09') {
                $validCountPlanIn['09:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
            } elseif ($tSecInHour == '10') {
                $validCountPlanIn['10:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
            } elseif ($tSecInHour == '11') {
                $validCountPlanIn['11:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
            } elseif ($tSecInHour == '12') {
                $validCountPlanIn['12:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
            } elseif ($tSecInHour == '13') {
                $validCountPlanIn['13:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
            } elseif ($tSecInHour == '14') {
                $validCountPlanIn['14:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
            } elseif ($tSecInHour == '15') {
                $validCountPlanIn['15:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
            } elseif ($tSecInHour == '16') {
                $validCountPlanIn['16:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
            } elseif ($tSecInHour == '17') {
                $validCountPlanIn['17:00']++; // Tambahkan 1 ke counter jika $countTruckIn['tSecIn'] pada jam 08:00 hingga 08:59
            }
        }

        $hourlyDataPlan = [
            '07:00' => $validCountPlanIn['07:00'],
            '08:00' => $validCountPlanIn['08:00'],
            '09:00' => $validCountPlanIn['09:00'],
            '10:00' => $validCountPlanIn['10:00'],
            '11:00' => $validCountPlanIn['11:00'],
            '12:00' => $validCountPlanIn['12:00'],
            '13:00' => $validCountPlanIn['13:00'],
            '14:00' => $validCountPlanIn['14:00'],
            '15:00' => $validCountPlanIn['15:00'],
            '16:00' => $validCountPlanIn['16:00'],
            '17:00' => $validCountPlanIn['17:00'],
        ];
        // dd($hourlyData);

        $filterRangeTimeTruck = [];
        foreach ($filteredResults as $v) {
            if ($v['dSecIn'] == '01-01-1980' && $v['dSecOut'] == '01-01-1980' && $v['dSecIn'] == $past && $v['dSecOut'] == $past || $v['dSecOut'] == '01-01-1980' || $v['dSecIn'] == '01-01-1980' || $v['dSecOut'] == $past || $v['dSecIn'] == $past) {
                continue;
            } else {
                $filterRangeTimeTruck[] = [
                    'nama_supplier' => $v['nama_supplier'],
                    'timeDiff' => $v['timeDiff'],
                    'platNo' => $v['platNo']
                ];
            }
        }

        usort($filterRangeTimeTruck, function ($a, $b) {
            return $b['timeDiff'] <=> $a['timeDiff'];
        });

        $sortedFilterRangeTimeTruck = $filterRangeTimeTruck;

        //Rata-rata
        $averageRangeTimeSup = [];

        foreach ($filterRangeTimeTruck as $v) {
            $suppliers = $v['nama_supplier'];
            $timeDiff = isset($v['timeDiff']) ? $v['timeDiff'] : 0; // Mengatasi jika $v['timeDiff'] tidak ada

            if (in_array($suppliers, array_column($averageRangeTimeSup, 'nama_supplier'))) {
                foreach ($averageRangeTimeSup as &$supplierData) {
                    if ($supplierData['nama_supplier'] === $suppliers) {
                        $supplierData['countSupp'] += $timeDiff;
                        $supplierData['numOccurrences']++;
                        break;
                    }
                }
            } else {
                $averageRangeTimeSup[] = [
                    'nama_supplier' => $suppliers,
                    'countSupp' => $timeDiff,
                    'numOccurrences' => 1
                ];
            }
        }

        // Menghitung rata-rata (average) untuk setiap supplier
        foreach ($averageRangeTimeSup as &$supplierData) {
            $averageTimeDiff = ($supplierData['numOccurrences'] > 0) ? ($supplierData['countSupp'] / $supplierData['numOccurrences']) : 0;
            $supplierData['averageTimeDiff'] = $averageTimeDiff;
        }

        // Menghapus variabel referensi agar tidak mengganggu variabel lainnya
        unset($supplierData);

        // Menyimpan hasil rata-rata untuk setiap supplier dalam array asosiatif baru
        $averageResults = [];
        foreach ($averageRangeTimeSup as $supplierData) {
            $averageResults[] = [
                'nama_supplier' => $supplierData['nama_supplier'],
                'averageTimeDiff' => $supplierData['averageTimeDiff']
            ];
        }

        $NGDatas = [];

        foreach ($formattedDatas as $v) {
            if ($v['dSecIn'] !== '01-01-1970' && $v['dSecIn'] !== '01-01-1980') {
                if ($v['onTime'] == false) {
                    $supplierName = $v['nama_supplier'];

                    if (isset($NGDatas[$supplierName])) {
                        $NGDatas[$supplierName]++;
                    } else {
                        $NGDatas[$supplierName] = 0;
                    }
                }
            }
        }
        // dd($filteredResults);

        $allData = [
            'NGDatas' => $NGDatas,
            'averageResults' => $averageResults,
            'formattedDatas' => $formattedDatas,
            'formattedDatasTruck' => $filteredResults,
            'filterRangeTimeTruck' => $sortedFilterRangeTimeTruck,
            'count' => $count ?? 0,
            'filterSup' => $filterSup,
            'filterTime' => $filterTime,
            'dateNow' => $dateNow ?? date('d-M-Y'),
            'past' => $past ?? date('d-m-Y', strtotime('-1 day')),
            'date' => $date,
            'uniquePlat' => $uniquePlatNo,
            'hourlyData' => $hourlyData,
            'hourlyDataPlan' => $hourlyDataPlan
        ];

        return $allData;
    }

    public function index()
    {
        $allDatas = $this->suppriler();
        return view('pages/dashboard', $allDatas);
    }

    public function listTruck()
    {
        $allDatas = $this->suppriler();
        // dd($allDatas);
        return view('pages/listTruck', $allDatas);
    }

    public function allDailyCharts()
    {
        $allDatas = $this->suppriler();
        // dd($allDatas['formattedDatasTruck']);
        return view('pages/charts', $allDatas);
    }
}
