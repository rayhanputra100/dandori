<?php

namespace App\Controllers;

use App\Models\TTemp;
use App\Models\TLogSupplier;
use App\Models\API;
use PHPUnit\Util\Json;
date_default_timezone_set("Asia/Jakarta");
class Temp extends BaseController
{
    protected $tempModel;
    protected $logSupplierModel;
    protected $apiModel;

    public function __construct()
    {
        $this->tempModel = new TTemp();
        $this->logSupplierModel = new TLogSupplier();
        $this->apiModel = new API();
    }

    public function sendTempData()
    {
        date_default_timezone_set("Asia/Jakarta");
        $date = null ? date('Y/m/d') : date('Y/m/d');
        $filterDate = date('Y/m/d', strtotime($date));
        $totalExistingData = 0;
        $totalNotExistingData = 0;

        $dataFromAPI = $this->apiModel->getItem($filterDate);

        foreach ($dataFromAPI as $datas) {
            foreach ($datas['results'][0] as $result) {
                // Check if data exists in the database based on 'no_dn' value
                $existingData = $this->tempModel->where('no_dn', $result['no_dn'])->first();

                if ($existingData) {
                    $totalExistingData++;
                    // Data already exists, perform update
                    $this->tempModel->update(['id' => $existingData['id']], [
                        'no_po' => $result['no_po'],
                        'date_delivery_plan' => $result['date_delivery_plan'],
                        'confirm_delivery_plan' => $result['confirm_delivery_plan'],
                        'date_done' => $result['date_done'],
                        'qa_date' => $result['qa_date'],
                        'warehouse_date' => $result['warehouse_date'],
                        'receipt_security' => $result['receipt_security'],
                        'kendaraan_polisi' => $result['kendaraan_polisi'],
                        'driver' => $result['driver'],
                        'nama_supplier' => $result['nama_supplier'],
                        'URGENT' => $result['URGENT'],
                        'receipt_standard' => $result['receipt_standard']
                    ]);
                } else {
                    $totalNotExistingData++;
                    // Data does not exist, perform save
                    $this->tempModel->save([
                        'no_dn' => $result['no_dn'],
                        'no_po' => $result['no_po'],
                        'date_delivery_plan' => $result['date_delivery_plan'],
                        'confirm_delivery_plan' => $result['confirm_delivery_plan'],
                        'date_done' => $result['date_done'],
                        'qa_date' => $result['qa_date'],
                        'warehouse_date' => $result['warehouse_date'],
                        'receipt_security' => $result['receipt_security'],
                        'kendaraan_polisi' => $result['kendaraan_polisi'],
                        'driver' => $result['driver'],
                        'nama_supplier' => $result['nama_supplier'],
                        'URGENT' => $result['URGENT'],
                        'receipt_standard' => $result['receipt_standard']
                    ]);
                }
            }
        }
        // Combine log messages using sprintf and log_message
        $data = $this->logSupplierModel->save([
            'log_save' => $totalNotExistingData,
            'log_update' => $totalExistingData
        ]);
        
        return $data;
    }
}
