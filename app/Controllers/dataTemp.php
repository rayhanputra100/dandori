<?php

namespace App\Controllers;

use App\Models\TArea;
use App\Models\API;
use App\Models\TScan;
use App\Models\TTemp;
use App\Models\TScanUnloading;
use \DateTime;
use \DateTimeZone;

class dataTemp extends BaseController
{
    protected $scanModel;
    protected $scanUnloadingModel;
    protected $areaModel;
    protected $apiModel;
    protected $tempModel;
    public function __construct()
    {
        $this->areaModel = new TArea();
        $this->apiModel = new API();
        $this->scanUnloadingModel = new TScanUnloading();
        $this->scanModel = new TScan();
        $this->tempModel = new TTemp();
    }

    private function suppriler()
    {
        $date = $this->request->getGet('date') == null ? date('Y/m/d') : $this->request->getGet('date');
        $filterDate = date('Y/m/d', strtotime($date));
        $filterMonth = strtoupper(date('M', strtotime($date)));
        $formattedDatas = []; //array penampung data yang sudah diformat
        $uniquePlatNo = []; //array penampung nama supplier yang sudah unik
        //melakukan iterasi terhadap hasil fetch data dari supplier

        $filteredResults = [];
        $drivers = [];
        $filterTime = [];
        $filterSup = [];
        // dd($this->tempModel->getItem());

        foreach ($this->tempModel->getItem() as $datTruck) {

            $platNo = $datTruck['kendaraan_polisi'];
            //jika nama supplier belum ada di dalam array uniqueSupplierNames, maka masukkan ke dalamnya
            if (!in_array($platNo, $uniquePlatNo)) {
                $uniquePlatNo[] = $platNo; //tambahkan ke array uniqueSupplierNames
            } else {
                continue; //jika nama supplier sudah ada di dalam array, maka lanjut ke iterasi selanjutnya
            }
        }

        foreach ($this->tempModel->getItem() as $datSupplier) {

            $date = $this->request->getGet('date');
            $filterDate = $date != null ? date('d-m-Y', strtotime($date)) : null;
            $past = $filterDate != null ? date('d-m-Y', strtotime('-1 day', strtotime($filterDate))) : date('d-m-Y', strtotime('-1 day'));
            if (date_format(date_create($datSupplier['confirm_delivery_plan']), 'd-m-Y') == date_format(date_create($datSupplier['date_delivery_plan']), 'd-m-Y')) {
                // $url = 'https://portal2.incoe.astra.co.id/vendor_rating/api/dn_json/' . $datSupplier['no_dn'];
                // $response = file_get_contents($url);
                // $data = json_decode($response, true);
                $datSupplierName = $datSupplier['nama_supplier'];
                $limTime = "16-MAR-2023 00:30";
                $fLimTime = date_create($limTime);
                $timePlanIn = date_format(date_create($datSupplier['confirm_delivery_plan']), 'H:i') != '00:00' ? date_create($datSupplier['confirm_delivery_plan']) : date_create($datSupplier['date_delivery_plan']);
                $fTimePlanIn = date_format(date_create($datSupplier['confirm_delivery_plan']), 'H:i') != '00:00' ? date("H", strtotime($datSupplier['confirm_delivery_plan'])) * 60 + date("i", strtotime($datSupplier['confirm_delivery_plan'])) : date("H", strtotime($datSupplier['date_delivery_plan'])) * 60 + date("i", strtotime($datSupplier['date_delivery_plan']));
                $fTimeSecIn = date("H", strtotime($datSupplier['receipt_security'])) * 60 + date("i", strtotime($datSupplier['receipt_security']));
                $fTimeSecOut = date("H", strtotime($datSupplier['date_done'])) * 60 + date("i", strtotime($datSupplier['date_done']));
                // $ftimeScan = date("H", strtotime($timeScan)) * 60 + date("i", strtotime($timeScan));
                $fTimeQC = date("H", strtotime($datSupplier['qa_date'])) * 60 + date("i", strtotime($datSupplier['qa_date']));
                $fTimeWH = date("H", strtotime($datSupplier['warehouse_date'])) * 60 + date("i", strtotime($datSupplier['warehouse_date']));

                $timeMin = $timePlanIn->getTimestamp() - $fLimTime->getTimestamp();
                $timeMax = $timePlanIn->getTimestamp() + $fLimTime->getTimestamp();
                // $total_minutes = date("H", strtotime($timeDiff)) * 60 + date("i", strtotime($timeDiff));
                $timeMaxFormatted = date_create('@' . $timeMax)->format('H:i');
                $timeMinFormatted = date_create('@' . $timeMin)->format('H:i');
                $dateNow = date_format(date_create($datSupplier['confirm_delivery_plan']), 'd-M-Y');

                $formattedData = []; //array penampung data supplier yang sudah diformat

                //melakukan iterasi terhadap hasil fetch data dari TArea
                foreach ($this->areaModel->getItem() as $areaItem) {
                    if ($datSupplierName === $areaItem['name_supplier']) {
                        $status = array_column($this->scanModel->getItem(), 'status');
                        $noDNs = array_column($this->scanModel->getItem(), 'noDN');
                        $key = array_search($datSupplier['no_dn'], $noDNs);

                        $statusUnloading = array_column($this->scanUnloadingModel->getItem(), 'status');
                        $noDNUnloading = array_column($this->scanUnloadingModel->getItem(), 'noDN');
                        $keyUnloading = array_search($datSupplier['no_dn'], $noDNUnloading);

                        if ($keyUnloading !== false) {
                            $datasUnloading = $datSupplier['no_dn']; //tambahkan ke array uniqueSupplierNames
                            $dTimeScanUnloading = $this->scanUnloadingModel->getItem()[$keyUnloading]['created_at'];
                            $timeScanUnloading = (new DateTime($dTimeScanUnloading))->setTimezone(new DateTimeZone('Asia/Jakarta'))->format('H:i');
                            $ftimeScanUnloading = date("H", strtotime($timeScanUnloading)) * 60 + date("i", strtotime($timeScanUnloading));
                        } else {
                            $datasUnloading = "-";
                            $timeScanUnloading = "-";
                            $ftimeScanUnloading = 0;
                        }

                        if ($key !== false) {
                            $datas = $datSupplier['no_dn']; //tambahkan ke array uniqueSupplierNames
                            $dTimeScanQCR = $this->scanModel->getItem()[$key]['created_at'];
                            $timeScan = (new DateTime($dTimeScanQCR))->setTimezone(new DateTimeZone('Asia/Jakarta'))->format('H:i');
                            $ftimeScan = date("H", strtotime($timeScan)) * 60 + date("i", strtotime($timeScan));
                        } else {
                            $datas = "-";
                            $timeScan = "-";
                            $ftimeScan = 0;
                        }

                        $formattedData = [
                            'tPlanIn' => date_format(date_create($datSupplier['confirm_delivery_plan']), 'H:i') != '00:00' ? date_format(date_create($datSupplier['confirm_delivery_plan']), 'H:i') : date_format(date_create($datSupplier['date_delivery_plan']), 'H:i'),
                            'tSecIn' => date_format(date_create($datSupplier['receipt_security']), 'H:i'),
                            'tQC' => date_format(date_create($datSupplier['qa_date']), 'H:i'),
                            'tWHC' => date_format(date_create($datSupplier['warehouse_date']), 'H:i'),
                            'tSecOut' => date_format(date_create($datSupplier['date_done']), 'H:i'),
                            'dPlanIn' => date_format(date_create($datSupplier['confirm_delivery_plan']), 'd-m-Y'),
                            'dSecIn' => date_format(date_create($datSupplier['receipt_security']), 'd-m-Y'),
                            'dQC' => date_format(date_create($datSupplier['qa_date']), 'd-m-Y'),
                            'dWHC' => date_format(date_create($datSupplier['warehouse_date']), 'd-m-Y'),
                            'dSecOut' => date_format(date_create($datSupplier['date_done']), 'd-m-Y'),
                            'nama_supplier' => $datSupplierName,
                            'noDN' => $datSupplier['no_dn'],
                            'priority' => $datSupplier['URGENT'],
                            'linkItem' => 'https://portal2.incoe.astra.co.id/vendor_rating_infor/api/dn_json/' . $datSupplier['no_dn'],
                            'area' => $areaItem['area'],
                            'platNo' => $datSupplier['kendaraan_polisi'],
                            'timeDiff' => $fTimeSecOut - $fTimeSecIn,
                            'timeDiffSecIn' => $fTimeSecIn - $fTimePlanIn,
                            'timeDiffQC' => ((date_format(date_create($datSupplier['receipt_security']), 'd-m-Y') == '01-01-1980') || (date_format(date_create($datSupplier['receipt_security']), 'd-m-Y') == $past) ? 0 : $fTimeQC - $fTimeSecIn),
                            'timeDiffWHC' => ((date_format(date_create($datSupplier['qa_date']), 'd-m-Y') == '01-01-1980') || (date_format(date_create($datSupplier['qa_date']), 'd-m-Y') == $past) ? 0 : $fTimeWH - $fTimeQC),
                            'timeDiffSecOut' => ((date_format(date_create($datSupplier['warehouse_date']), 'H:i') == '07:00') || (date_format(date_create($datSupplier['warehouse_date']), 'd-m-Y') == $past) ? 0 : $fTimeSecOut - $fTimeWH),
                            'nDriver' => $datSupplier['driver'],
                            'max' => $timeMaxFormatted,
                            'min' => $timeMinFormatted,
                            'statusUnloading' => $statusUnloading,
                            'tScanUnloading' => $timeScanUnloading,
                            'dnScanUnloading' => $datasUnloading,
                            'ftimeScanUnloading' => $ftimeScanUnloading,
                            'status' => $status,
                            'tScanQCR' => $timeScan,
                            'dnScan' => $datas,
                            'ftimeScan' => $ftimeScan,
                            'fTimeQC' => $fTimeQC,
                            'fTimeWHC' => $fTimeWH,
                            'fTimeSecIn' => $fTimeSecIn

                            // 'iQTY' => $fixData['QTY'],
                            // 'iPONO' => $fixData['PONO'],
                            // 'iITEM' => $fixData['ITEM'],
                            // 'iPO' => $fixData['PO'],
                            // 'count' => $count,
                        ];
                        break;
                    }
                }
                //jika formattedData tidak kosong, maka masukkan data tersebut ke array formattedDatas
                if (!empty($formattedData)) {

                    //jika formattedData tidak kosong, maka masukkan data tersebut ke array formattedDatas
                    $scanSecIn = $formattedData['dSecIn'] == '01-01-1970' || $formattedData['dSecIn'] == '01-01-1980' || $formattedData['dSecIn'] == $past;
                    $scanQcRec = $formattedData['noDN'] != $formattedData['dnScan'] || $formattedData['status'] == 'false';
                    $scanQc = $formattedData['dQC'] == '01-01-1980' || $formattedData['dQC'] == $past;
                    $scanUnloadingFinish = $formattedData['noDN'] != $formattedData['dnScanUnloading'] || $formattedData['statusUnloading'] == 'false';
                    $scanWhc = $formattedData['tWHC'] == '07:00' || $formattedData['dWHC'] == $past;
                    $scanSecOut = $formattedData['dSecOut'] == '01-01-1980' || $formattedData['dSecOut'] == $past;

                    $formattedData['onTime'] =
                        $formattedData['tSecIn'] <= $formattedData['max']
                        && $formattedData['tSecIn'] >= $formattedData['min']
                        // && $formattedData['tSecIn'] <= $formattedData['tScanQCR']
                        // && $formattedData['tSecIn'] <= $formattedData['tQC']
                        // && $formattedData['tSecIn'] <= $formattedData['tWHC']
                        // && $formattedData['tSecIn'] <= $formattedData['tScanUnloading']
                        && $formattedData['tSecIn'] <= $formattedData['tSecOut']

                        // && $formattedData['tScanQCR'] >= $formattedData['tSecIn']
                        // && $formattedData['tScanQCR'] <= $formattedData['tQC']
                        // && $formattedData['tScanQCR'] <= $formattedData['tWHC']
                        // && $formattedData['tScanQCR'] <= $formattedData['tScanUnloading']
                        // && $formattedData['tScanQCR'] <= $formattedData['tSecOut']

                        // && $formattedData['tQC'] >= $formattedData['tSecIn']
                        // && $formattedData['tQC'] >= $formattedData['tScanQCR']
                        // && $formattedData['tQC'] <= $formattedData['tWHC']
                        // && $formattedData['tQC'] <= $formattedData['tScanUnloading']
                        && $formattedData['tQC'] <= $formattedData['tSecOut']

                        // && $formattedData['tScanUnloading'] >= $formattedData['tSecIn']
                        // && $formattedData['tScanUnloading'] >= $formattedData['tScanQCR']
                        // && $formattedData['tScanUnloading'] >= $formattedData['tQC']
                        // && $formattedData['tScanUnloading'] <= $formattedData['tWHC']
                        // && $formattedData['tScanUnloading'] <= $formattedData['tSecOut']

                        // && $formattedData['tWHC'] >= $formattedData['tSecIn']
                        // && $formattedData['tWHC'] >= $formattedData['tScanQCR']
                        // && $formattedData['tWHC'] >= $formattedData['tQC']
                        // && $formattedData['tWHC'] >= $formattedData['tScanUnloading']
                        && $formattedData['tWHC'] <= $formattedData['tSecOut']

                        && $formattedData['tSecOut'] >= $formattedData['tSecIn']
                        // && $formattedData['tSecOut'] >= $formattedData['tScanQCR']
                        && $formattedData['tSecOut'] >= $formattedData['tWHC']
                        && $formattedData['tSecOut'] >= $formattedData['tQC'];
                    // && $formattedData['tSecOut'] >= $formattedData['tScanUnloading'];

                    $formattedData['onTimeWH'] =
                        $formattedData['tWHC'] <= $formattedData['tSecOut']
                        && $formattedData['tSecOut'] >= $formattedData['tWHC'];

                    $formattedData['onTimeQC'] =
                        $formattedData['tQC'] <= $formattedData['tSecOut']
                        && $formattedData['tSecOut'] >= $formattedData['tQC'];

                    //JUST SEE THE DATA
                    // $formattedData['notScan'] = [
                    //     'scanSecIn' => $scanSecIn,
                    //     'scanQcRec' => $scanQcRec,
                    //     'scanQc' => $scanQc,
                    //     'scanUnloadingFinish' => $scanUnloadingFinish,
                    //     'scanUnloadingFinish' => $scanUnloadingFinish,
                    //     'scanSecOut' => $scanSecOut
                    // ];

                    $formattedData['notScan'] =
                        ($scanSecIn == true
                            // || $scanQcRec == true
                            || $scanQc == true
                            // || $scanUnloadingFinish == true
                            || $scanWhc == true
                            || $scanSecOut == true)
                        && !($scanSecIn == false
                            // && $scanQcRec == false
                            && $scanQc == false
                            // && $scanUnloadingFinish == false
                            && $scanWhc == false
                            && $scanSecOut == false
                        );

                    $formattedData['outOffSchedule'] =
                        $formattedData['tSecIn'] <= $formattedData['max']
                        && $formattedData['tSecIn'] >= $formattedData['min'];

                    $formattedDatas[] = $formattedData;
                }
            }
        }


        // dd($formattedDatas);
        // Inisialisasi array untuk menyimpan data dalam tiap bulan
        $monthlyData = array(
            'JAN' => array(),
            'FEB' => array(),
            'MAR' => array(),
            'APR' => array(),
            'MAY' => array(),
            'JUN' => array(),
            'JUL' => array(),
            'AUG' => array(),
            'SEP' => array(),
            'OCT' => array(),
            'NOV' => array(),
            'DEC' => array(),
        );

        foreach ($formattedDatas as $data) {
            // Ambil bulan dari field dPlanIn (format '27-07-2023')
            $bulan = strtoupper(date_format(date_create($data['dPlanIn']), 'M'));

            // Masukkan data ke dalam array yang sesuai dengan bulan
            $monthlyData[$bulan][] = $data;
        }

        foreach ($monthlyData[$filterMonth] as $truck) {
            $driver = $truck['platNo'];
            if (!in_array($driver, $drivers)) {
                $filteredResults[] = $truck;
                $drivers[] = $driver;
            }
            $count = count($drivers); //untuk menghitung total truck
        }

        foreach ($monthlyData[$filterMonth] as $time) {
            $times = $time['tPlanIn'];
            if (!in_array($times, $filterTime)) {
                $filterTime[] = $times;
            }
        }

        foreach ($monthlyData[$filterMonth] as $sup) {
            $sups = $sup['nama_supplier'];
            if (!in_array($sups, $filterSup)) {
                $filterSup[] = $sups;
            }
        }

        $NGDatas = [];
        $detailNGDatas = [];
        $detailNGDatasYear = [];
        // dd($monthlyData);
        foreach ($monthlyData as $month => $data) {
            $filteredData = array_filter($data, function ($v) {
                $date = $this->request->getGet('date') == null ? date('Y/m/d') : $this->request->getGet('date');
                $filterYear = date('Y', strtotime($date));
                $vYear = date('Y', strtotime($v['dSecIn']));
                return $v['dSecIn'] !== '01-01-1970' && $v['dSecIn'] !== '01-01-1980' && $v['onTime'] == false && $vYear == $filterYear;
            });

            $monthData = [
                'date' => $month,
                'count' => count($filteredData),
                'details' => [
                    'AKR Corporindo, Tbk' => 0,
                    'Presisi Cileungsi Makmur, PT' => 0,
                    'Indonesian Acids Industry, PT' => 0,
                    'Inti Nomika Indonesia,PT' => 0,
                    'Non Ferindo Utama, PT' => 0,
                    'Presisi Cimanggis Makmur, PT' => 0,
                    'Peace Industrial Packaging, PT' => 0,
                    'Indra Eramulti Logam Industri, PT' => 0,
                    'Inti Lingga Sejahtera, PT' => 0,
                    'BEST LABEL. PT' => 0,
                    'Dinamika Polimerindo,PT' => 0,
                    'Dirga Buana Sarana, PT' => 0,
                    'Citra Union Inks, PT' => 0,
                    'Inti Karet, CV' => 0,
                    'Entek Separindo Asia, PT' => 0,
                    'Q Mould Plastik Indonesia PT' => 0,
                    'Bioplast Unggul, PT' => 0,
                    'Tenar Inti Mandiri,PT' => 0,
                    'Dinar Makmur,PT' => 0,
                    'Karta Jaya Karetindo, PT' => 0,
                    'Air Products Indonesia, PT' => 0,
                    'Indolabel Surya Pratama PT' => 0,
                    'Yasunli Abadi Utama Plastic, PT' => 0,
                    'Mahkota Jaya Raya PT' => 0,
                    'Sinar Utama Mandiri, PT' => 0,
                    'Piala Dunia ' => 0,
                    'Pura Barutama, PT    ' => 0,
                    'Pasifik Kimia Indonesia, PT' => 0,
                    'Mitra Sarana Sukses, CV' => 0,
                    'Karabha Wiratama, PT   '    => 0,
                    'BT Source Internasional'    => 0,
                    'Delico, PD' => 0,
                    'GS Battery, PT' => 0,
                    'Ligno Specialty Adhesive, PT' => 0,
                    'Metro Makmur Lestari, PT' => 0,
                    'Pash Mitra Mandiri, PT' => 0,
                    'Dynaplast, PT' => 0,
                    'Tugu Pakulonan, PT' => 0,
                    'Smart Insan Mandiri, PT' => 0,
                    'Anugrah Gasindo, PT' => 0,
                    'Karya Osaka, PD' => 0,
                    'Autoplastik Indonesia, PT' => 0,
                    'Dai Nippon Printing, PT' => 0,
                    'DIC Astra Chemicals,PT' => 0,
                    'Quantumplast Industry, PT' => 0,
                    'Sinjiwira Jaya Abadi, PT' => 0,
                    'Koperasi Karyawan Incoe' => 0,
                ]
            ];

            foreach ($filteredData as $v) {
                $supplierName = $v['nama_supplier'];

                if (!isset($monthData['details'][$supplierName])) {
                    $monthData['details'][$supplierName] = 1;
                } else {
                    $monthData['details'][$supplierName]++;
                }
            }

            $detailNGDatasYear[] = $monthData;
        }
        foreach ($monthlyData[$filterMonth] as $v) {
            if ($v['dSecIn'] !== '01-01-1970' && $v['dSecIn'] !== '01-01-1980') {
                if ($v['outOffSchedule'] == false) {

                    // $nameSup = $v['nama_supplier'];
                    $date = date('d-m', strtotime($v['dSecIn']));
                    $month = date('m', strtotime($v['dSecIn']));
                    $detailNGDatas[$date][] = $v;
                    // $detailNGDatasYear[] = $v;
                    // Mengecek apakah tanggal sudah ada di $NGDatas
                    if (isset($NGDatas[$date])) {
                        $NGDatas[$date]++;
                    } else {
                        $NGDatas[$date] = 1;
                    }
                }
            }
        }
        // dd($detailNGDatas);
        // Mengurutkan data berdasarkan tanggal secara ascending
        ksort($NGDatas);
        ksort($detailNGDatas);

        // Combine the $NGDatas and $detailNGDatas arrays
        $combinedNGDatas = [];
        foreach ($NGDatas as $date => $count) {
            $dataByDate = [
                'date' => $date,
                'count' => $count,
                'details' => []
            ];

            if (isset($detailNGDatas[$date])) {
                $details = $detailNGDatas[$date];
                foreach ($details as $detail) {
                    $dataByDate['details'][] = [
                        'nama_supplier' => $detail['nama_supplier'],
                        'platNo' => $detail['platNo'],
                        'tPlanIn' => $detail['tPlanIn'],
                        'tSecIn' => $detail['tSecIn'],
                        'max' => $detail['max'],
                        'min' => $detail['min'],
                        'timeDiffSecIn' => $detail['timeDiffSecIn'],
                    ];
                }
            }

            $combinedNGDatas[] = $dataByDate;
        }


        // $detailNGYear = [];
        // foreach($detailNGDatasYear as )
        // Output the result
        // dd($detailNGDatasYear);

        $allData = [
            'detailNGDatasYear' => $detailNGDatasYear,
            'combinedNGDatas' => $combinedNGDatas,
            // 'averageResults' => $averageResultsPerDay,
            'formattedDatas' => $formattedDatas,
            'date' => $date,
        ];

        return $allData;
    }

    public function index()
    {
        $allDatas = $this->suppriler();
        // dd($allDatas);
        return view('pages/monthlyCharts', $allDatas);
    }
}
