<?php

namespace App\Controllers;

use App\Models\TScan;

class scanQCR extends BaseController
{
    protected $scanModel;
    public function __construct()
    {
        $this->scanModel = new TScan();
    }

    public function scan()
    {
        $postDN = strtoupper($this->request->getVar('noDN'));
        $existingData = $this->scanModel->where('noDN', $postDN)->first();

        $data = [
            'noDN' => $postDN,
            'status' => $postDN ? 'True' : 'False',
        ];

        if ($this->scanModel->save($data)) {
            if ($existingData) {
                session()->setFlashdata('warning', 'Data Sudah Ada');
            } else {
                session()->setFlashdata('success', 'Data Berhasil Di Input');
            }
        } else {
            session()->setFlashdata('danger', 'Data Gagal Di Input');
        }

        return redirect()->to(base_url('/qcreq'));
    }

    public function scanView()
    {
        return view('pages/scanDN');
    }
}
