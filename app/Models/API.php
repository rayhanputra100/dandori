<?php

namespace App\Models;

use CodeIgniter\Model;
use Config\Services;
use DateTime; // Tambahkan impor untuk namespace DateTime

class API extends Model
{
    public function getItem($date)
    {
        $client = Services::curlrequest();
        $dateParts = date_parse($date);

        $tahun = $dateParts['year'];
        $bulan = $dateParts['month'];
        $tanggal = $dateParts['day'];

        $url = "https://portal2.incoe.astra.co.id/vendor_rating_infor/api/Approve_json_backdate/" . $date;
        // $url = "https://portal2.incoe.astra.co.id:1310/get_vendor_incoming?hari=".$tanggal."&bulan=".$bulan."&tahun=".$tahun."";
        $response = $client->request('GET', $url);
        // var_dump($response );die();

        $data['supplier']['results'] = json_decode($response->getBody(), true);

        // Sorting data berdasarkan date_delivery_plan
        usort($data['supplier']['results'], function ($a, $b) {
            // Ubah format "27-JUL-2023 07:30" menjadi format angka "YYYYmmddHHii"
            $timeA = DateTime::createFromFormat('d-M-Y H:i', $a['date_delivery_plan'])->format('YmdHi');
            $timeB = DateTime::createFromFormat('d-M-Y H:i', $b['date_delivery_plan'])->format('YmdHi');

            // Lakukan perbandingan numerik berdasarkan waktu
            return $timeA - $timeB;
        });

        return $data;
    }
}
