<?php

namespace App\Models;

use CodeIgniter\Model;

class TArea extends Model
{
    // protected $table            = 'incoming_transit_area';
    protected $table            = 'T_Area';
    protected $allowedFields    = ['name_supplier', 'area', 'plan_in'];

    public function getItem()
    {
        return $this->findAll();
    }
}
