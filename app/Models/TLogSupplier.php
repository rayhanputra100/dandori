<?php

namespace App\Models;

use CodeIgniter\Model;

class TLogSupplier extends Model
{

    protected $table = 'TLogSupplier';
    protected $allowedFields = ['log_save','log_update'];
    protected $useTimestamps = true;

    public function getItem($id = false)
    {
        if($id == false){
            return $this->findAll();
        }

        return $this->where(['id' => $id])->first();
    }
}