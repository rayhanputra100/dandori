<?php

namespace App\Models;

use CodeIgniter\Model;

class TScan extends Model
{

    // protected $table = 'incoming_scan_supplier';
    protected $table = 'TScan';
    protected $allowedFields = ['noDN','status'];
    protected $useTimestamps = true;

    public function getItem($id = false)
    {
        if($id == false){
            return $this->findAll();
        }

        return $this->where(['id' => $id])->first();
    }
}