<?php

namespace App\Models;

use CodeIgniter\Model;

class TScanUnloading extends Model
{
    // protected $table = 'incoming_scan_unloading';
    protected $table = 'TScanUnloading';
    protected $allowedFields = ['noDN','status'];
    protected $useTimestamps = true;

    public function getItem($id = false)
    {
        if($id == false){
            return $this->findAll();
        }

        return $this->where(['id' => $id])->first();
    }
}