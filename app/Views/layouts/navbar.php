<!-- Navigation Bar-->
<header id="topnav">
    <div class="topbar-main">
        <div class="container-fluid">

            <!-- Logo container-->
            <div class="logo">

                <!-- Image Logo -->
                <a href="/" class="logo d-flex">
                    <img src="<?= base_url(); ?>assets/images/iPTCBI.png" alt="" height="80" class="logo-large">
                </a>

            </div>
            <h2 class="text-dark text-center pt-3 fs-1">DANDORI</h2>
            <!-- End Logo container-->
            <div class="clearfix"></div>

        </div> <!-- end container -->
    </div>
    <!-- end topbar-main -->

    <!-- MENU Start -->
    <div class="navbar-custom">
        <div class="container-fluid">
            <div id="navigation">
                <!-- Navigation Menu-->
                <ul class="navigation-menu">
                    <li class="has-submenu">
                        <a href="#"><i class="mdi mdi-airplay"></i>Dashboard</a>
                        <ul class="submenu">
                            <li><a href="<?php echo base_url(); ?>">Home</a></li>
                            <!-- <li><a href="<?php echo base_url(); ?>detail">Detail</a></li>
                            <li><a href="<?php echo base_url(); ?>daily_charts">Daily Charts</a></li>
                            <li><a href="<?php echo base_url(); ?>monthly_charts">Monthly Charts</a></li> -->
                        </ul>
                    </li>
                    <li class="has-submenu">
                        <a href="#"><i class="mdi mdi-qrcode-scan"></i>Scan</a>
                        <ul class="submenu">
                            <!-- <li><a target="_blank" href="https://portal2.incoe.astra.co.id/vendor_rating_infor/Security/Scan_come">Scan In</a></li>
                            <li><a target="_blank" href="https://portal2.incoe.astra.co.id/vendor_rating_infor/Security/Scan_out">Scan Out</a></li> -->
                            <li><a target="_blank" href="<?php echo base_url(); ?>unloading">Scan MTN</a></li>
                            <li><a target="_blank" href="<?php echo base_url(); ?>unloading">Scan Setting Prod</a></li>
                            <li><a target="_blank" href="<?php echo base_url(); ?>qcreq">Scan QC</a></li>

                        </ul>
                    </li>
                </ul>
                <!-- End navigation menu -->
            </div> <!-- end #navigation -->
        </div> <!-- end container -->
    </div> <!-- end navbar-custom -->
</header>