<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="Dashboard" />
    <meta content="PT CBI || Rayhan Putra" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>DANDORI</title>

    <!-- CSS -->
    <link rel="stylesheet" type="" href="<?= base_url(); ?>mycss/style.css">
    <link href="<?= base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css">

    <!-- Boostrap CSS -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/iPTCBI.ico">
</head>


<body>
    <?= $this->include('layouts/navbar.php'); ?>
    <?= $this->renderSection('content'); ?>

    <!-- Footer -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    © 2023 PT CBI.
                </div>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <!-- jQuery  -->
    <script src="<?= base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/popper.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/modernizr.min.js"></script>
    <script src="<?= base_url(); ?>assets/js/waves.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.slimscroll.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.nicescroll.js"></script>
    <script src="<?= base_url(); ?>assets/js/jquery.scrollTo.min.js"></script>

    <!-- Plugins Init js -->
    <script src="<?= base_url(); ?>assets/pages/form-advanced.js"></script>
    <!-- Datatable init js -->
    <script src="<?= base_url(); ?>assets/js/tableHTMLExport.js"></script>

    <script>
        $(document).ready(function() {
            $('.bs-example-modal-lg').on('shown.bs.modal', function() {
                $('#noDN').focus();
            });
        });
    </script>
</body>

</html>