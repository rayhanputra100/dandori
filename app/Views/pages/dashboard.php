<?= $this->extend('layouts/template/template'); ?>

<?= $this->section('content'); ?>


<!-- Wrapper -->
<div class="wrapper mt-3">

    <!-- Container -->
    <div class="container-fluid">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title fs-4">Data Kedatangan Supplier</h4>
                </div>
            </div>
        </div>
        <!-- End Page-Title -->

        <!-- Table -->
        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title fs-5">LIST SUPPLIER (<?= $dateNow ?>)</h2>
                            <h4 class="mt-0 header-title fs-6">TOTAL TRUCK : <?= $count ?> </h4>
                            <div class="row">
                                <div class="col-2" style="margin:0">
                                    <div class="row">
                                        <div class="col">
                                            <form action="<?= site_url('/detail?date=' . $date) ?>" method="get">
                                                <label for="filter" class="form-label mb-1">Filter</label>
                                                <input type="text" class="form-control mb-2" name="date" placeholder="<?= $date == null ? date('Y/m/d') : $filterDate = date('Y/m/d', strtotime($date)); ?>" id="mdate">
                                                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-filter"></i> Filter</button>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="row mt-2">
                                        <div class="col">
                                            <label for="filter" class="form-label mb-2">Check Supplier</label>
                                            <select class="form-control mb-3" id="pickSup" name="pickSup">
                                                <option value="">-Default-</option>
                                                <?php foreach ($filterSup as $v) : ?>
                                                    <option value="<?= $v; ?>"><?= $v; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <!-- Modal -->
                                        <div class="modal fade" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="dataModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-xl">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title text-center" id="dataModalLabel">Data Supplier</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Nama Supplier: <span id="namaSup"></span></p>
                                                        <p>Jumlah Truck: <span id="jumlahTruck"></span></p>
                                                        <p>Jadwal Kedatangan Truck: <span id="jadwalTruck"></span></p>
                                                        <p>Status : </p>
                                                        <table class="bordered m-auto">
                                                            <thead class="bordered">
                                                                <th class="text-center px-3 bordered">No DN</th>
                                                                <th class="text-center px-3 bordered">Plan In</th>
                                                                <th class="text-center px-3 bordered">Sec In</th>
                                                                <th class="text-center px-3 bordered">QC Request</th>
                                                                <th class="text-center px-3 bordered">QC Finished</th>
                                                                <th class="text-center px-3 bordered">Unloading Finished</th>
                                                                <th class="text-center px-3 bordered">WHC</th>
                                                                <th class="text-center px-3 bordered">Sec Out</th>
                                                                <th class="text-center px-3 bordered">Range Time</th>
                                                            </thead>
                                                            <tbody id="dataBody"></tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-1">
                                        <div class="col">
                                            <a href="<?= site_url('export?date=' . $date) ?>" class="btn btn-success btn-sm my-4"><i class="fa fa-file-excel-o"></i> Download</a>
                                        </div>
                                    </div>

                                </div>
                                <div class="col d-flex justify-content-center">
                                    <div class="border-0 p-2 rounded-4 my-bg-dark">
                                        <figure class="highcharts-figure">
                                            <div id="containerTruck"></div>
                                        </figure>
                                    </div>
                                </div>
                            </div>

                            <div class="table-responsive b-0">
                                <table id="datatable" class="table table-bordered">
                                    <thead class="colorGrey">
                                        <tr>
                                            <th class="text-center text-color-head" scope="col">No</th>
                                            <th class="text-center text-color-head" scope="col">Name Supplier</th>
                                            <th class="text-center text-color-head" scope="col">license Plate</th>
                                            <th class="text-center text-color-head" scope="col">Driver</th>
                                            <th class="text-center text-color-head" scope="col">Delivery</th>
                                            <th class="text-center text-color-head" scope="col">Priority</th>
                                            <th class="text-center text-color-head" scope="col">Area</th>
                                            <th class="text-center text-color-head" scope="col">Plan In</th>
                                            <th class="text-center text-color-head" scope="col">Sec In</th>
                                            <th class="text-center text-color-head" scope="col">QC Request</th>
                                            <th class="text-center text-color-head" scope="col">QC Finished</th>
                                            <th class="text-center text-color-head" scope="col">Unloading Finished</th>
                                            <th class="text-center text-color-head" scope="col">WHC</th>
                                            <th class="text-center text-color-head" scope="col">Sec Out</th>
                                            <th class="text-center text-color-head" scope="col">Range Time</th>
                                            <th class="text-center text-color-head" scope="col">Result</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php if (!empty($formattedDatas)) : ?>
                                            <?php $i = 1; ?>
                                            <?php foreach ($formattedDatas as $formattedData) : ?>
                                                <tr>
                                                    <th scope="row" class="text-center text-color-head"><?= $i++ ?></th>
                                                    <td class="text-color-head"><?= $formattedData['nama_supplier'] ?></td>
                                                    <td class="text-color-head text-center"><?= $formattedData['platNo'] ?></td>
                                                    <td class="text-color-head text-center"><?= $formattedData['nDriver'] ?></td>
                                                    <td class="text-center text-color-head"><?= $formattedData['noDN'] ?></td>
                                                    <td class="text-center text-color-head <?= $formattedData['priority'] == NULL ? 'colorGreen' : 'colorRed'; ?> "><?= $formattedData['priority'] == NULL ? 'NORMAL' : 'URGENT'; ?></td>
                                                    <td id="area" class="text-center text-color-head"><?= $formattedData['area'] ?></td>
                                                    <td class="text-center text-color-head"><?= $formattedData['tPlanIn'] ?></td>
                                                    <?php
                                                    if ($formattedData['dSecIn'] == '01-01-1970' || $formattedData['dSecIn'] == '01-01-1980' || $formattedData['dSecIn'] == $past) {
                                                        echo '<td class="text-center text-color-head">' . '-' . '</td>';
                                                    } else {
                                                        echo '<td class="text-center">' . '<button type="button" class="btn btn-sm text-center text-color-head ' . ($formattedData['tSecIn'] <= $formattedData['max'] && $formattedData['tSecIn'] >= $formattedData['min'] ? 'colorGreen' : 'colorRed') . '" role="button" data-toggle="popover" data-trigger="focus" title="Range Time" data-content="Sec In to Plan In : ' . $formattedData['timeDiffSecIn'] . '">' . $formattedData['tSecIn'] . '</button>' . '</td>';
                                                    }
                                                    ?>

                                                    <?php
                                                    if ($formattedData['noDN'] != $formattedData['dnScan'] || $formattedData['status'] == 'false') {
                                                        echo '<td class="text-center text-color-head">' . '-' . '</td>';
                                                    } else {
                                                        // echo '<td class="text-center">' . '<button type="button" class="btn btn-sm text-center text-color-head ' . ($formattedData['tScanQCR'] >= $formattedData['tSecIn'] ? 'colorGreen' : 'colorRed') . '" role="button" data-toggle="popover" data-trigger="focus" title="Range Time" data-content="QC Request to Sec In : ' . $formattedData['ftimeScan'] - $formattedData['fTimeSecIn'] . '">' . $formattedData['tScanQCR'] . '</button>' . '</td>';
                                                        echo '<td class="text-center">' . '<button type="button" class="btn btn-sm text-center text-color-head colorGreen role="button" data-toggle="popover" data-trigger="focus" title="Range Time" data-content="QC Request to Sec In : ' . $formattedData['ftimeScan'] - $formattedData['fTimeSecIn'] . '">' . $formattedData['tScanQCR'] . '</button>' . '</td>';
                                                    }
                                                    ?>

                                                    <?php
                                                    if ($formattedData['dQC'] == '01-01-1980' || $formattedData['dQC'] == $past) {
                                                        echo '<td class="text-center text-color-head">' . '-' . '</td>';
                                                    } else {
                                                        // echo '<td class="text-center">' . '<button type="button" class="btn btn-sm text-center text-color-head ' . ($formattedData['tQC'] >= $formattedData['tSecIn'] && $formattedData['tQC'] <= $formattedData['tWHC'] && $formattedData['tQC'] <= $formattedData['tSecOut'] && $formattedData['tQC'] <= $formattedData['tScanUnloading'] && $formattedData['tQC'] >= $formattedData['tScanQCR'] ? 'colorGreen' : 'colorRed') . '" role="button" data-toggle="popover" data-trigger="focus" title="Range Time" data-content="QC Finished to QC Request : ' . $formattedData['fTimeQC'] - $formattedData['ftimeScan'] . '">' . $formattedData['tQC'] . '</button>' . '</td>';
                                                        echo '<td class="text-center">' . '<button type="button" class="btn btn-sm text-center text-color-head ' . ($formattedData['tQC'] <= $formattedData['tSecOut'] ? 'colorGreen' : 'colorRed') . '" role="button" data-toggle="popover" data-trigger="focus" title="Range Time" data-content="QC Finished to QC Request : ' . $formattedData['fTimeQC'] - $formattedData['ftimeScan'] . '">' . $formattedData['tQC'] . '</button>' . '</td>';
                                                    }
                                                    ?>

                                                    <?php
                                                    if ($formattedData['noDN'] != $formattedData['dnScanUnloading'] || $formattedData['statusUnloading'] == 'false') {
                                                        echo '<td class="text-center text-color-head">' . '-' . '</td>';
                                                    } else {
                                                        // echo '<td class="text-center">' . '<button type="button" class="btn btn-sm text-center text-color-head ' . ($formattedData['tScanUnloading'] <= $formattedData['tWHC'] ? 'colorGreen' : 'colorRed') . '" role="button" data-toggle="popover" data-trigger="focus" title="Range Time" data-content="Unloading Finished to QC Finished : ' . $formattedData['ftimeScanUnloading'] - $formattedData['fTimeQC'] . '">' . $formattedData['tScanUnloading'] . '</button>' . '</td>';
                                                        echo '<td class="text-center">' . '<button type="button" class="btn btn-sm text-center text-color-head colorGreen role="button" data-toggle="popover" data-trigger="focus" title="Range Time" data-content="Unloading Finished to QC Finished : ' . $formattedData['ftimeScanUnloading'] - $formattedData['fTimeQC'] . '">' . $formattedData['tScanUnloading'] . '</button>' . '</td>';
                                                    }
                                                    ?>

                                                    <?php
                                                    if ($formattedData['tWHC'] == '07:00' || $formattedData['dWHC'] == $past) {
                                                        echo '<td class="text-center text-color-head">' . '-' . '</td>';
                                                    } else {
                                                        // echo '<td class="text-center">' . '<button type="button" class="btn btn-sm text-center text-color-head ' . ($formattedData['tWHC'] >= $formattedData['tSecIn'] && $formattedData['tWHC'] >= $formattedData['tQC'] && $formattedData['tWHC'] >= $formattedData['tScanUnloading'] && $formattedData['tWHC'] <= $formattedData['tSecOut'] ? 'colorGreen' : 'colorRed') . '" role="button" data-toggle="popover" data-trigger="focus" title="Range Time" data-content="WHC to Unloading Finished : ' . $formattedData['fTimeWHC'] - $formattedData['ftimeScanUnloading'] . '">' . $formattedData['tWHC'] . '</button>' . '</td>';
                                                        echo '<td class="text-center">' . '<button type="button" class="btn btn-sm text-center text-color-head ' . ($formattedData['tWHC'] <= $formattedData['tSecOut'] ? 'colorGreen' : 'colorRed') . '" role="button" data-toggle="popover" data-trigger="focus" title="Range Time" data-content="WHC to Unloading Finished : ' . $formattedData['fTimeWHC'] - $formattedData['ftimeScanUnloading'] . '">' . $formattedData['tWHC'] . '</button>' . '</td>';
                                                    }
                                                    ?>

                                                    <?php
                                                    if ($formattedData['dSecOut'] == '01-01-1980' || $formattedData['dSecOut'] == $past) {
                                                        echo '<td class="text-center text-color-head">' . '-' . '</td>';
                                                    } else {
                                                        // echo '<td class="text-center">' . '<button type="button" class="btn btn-sm text-center text-color-head ' . ($formattedData['tSecOut'] >= $formattedData['tQC'] && $formattedData['tSecOut'] >= $formattedData['tWHC'] && $formattedData['tSecOut'] >= $formattedData['tQC'] && $formattedData['tSecOut'] >= $formattedData['tScanUnloading'] ? 'colorGreen' : 'colorRed') . '" role="button" data-toggle="popover" data-trigger="focus" title="Range Time" data-content="Sec Out to WHC : ' . $formattedData['timeDiffSecOut'] . '">' . $formattedData['tSecOut'] . '</button>' . '</td>';
                                                        echo '<td class="text-center">' . '<button type="button" class="btn btn-sm text-center text-color-head ' . ($formattedData['tSecOut'] >= $formattedData['tQC'] && $formattedData['tSecOut'] >= $formattedData['tWHC'] ? 'colorGreen' : 'colorRed') . '" role="button" data-toggle="popover" data-trigger="focus" title="Range Time" data-content="Sec Out to WHC : ' . $formattedData['timeDiffSecOut'] . '">' . $formattedData['tSecOut'] . '</button>' . '</td>';
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($formattedData['dSecIn'] == '01-01-1980' && $formattedData['dSecOut'] == '01-01-1980' && $formattedData['dSecIn'] == $past && $formattedData['dSecOut'] == $past || $formattedData['dSecOut'] == '01-01-1980' || $formattedData['dSecIn'] == '01-01-1980' || $formattedData['dSecOut'] == $past || $formattedData['dSecIn'] == $past) {
                                                        echo '<td class="text-center text-color-head">' . '-' . '</td>';
                                                    } else {
                                                        echo '<td class="text-center text-color-head bg-primary">' . $formattedData['timeDiff'] . '</td>';
                                                    }
                                                    ?>
                                                    <?php if ($formattedData['dSecIn'] == $past && $formattedData['dSecOut'] == $past && $formattedData['dWHC'] == $past && $formattedData['dQC'] == $past && $formattedData['notScan'] == true) : ?>
                                                        <td class="text-center">
                                                            <button type="button" class="btn btn-sm text-center text-color-head waves-effect waves-light" onclick="openModal('<?= $formattedData['platNo'] ?>')">-</button>
                                                        </td>
                                                    <?php elseif ($formattedData['notScan'] == true) : ?>
                                                        <td class="text-center">
                                                            <button type="button" class="btn btn-sm text-center text-color-head waves-effect waves-light" onclick="openModal('<?= $formattedData['platNo'] ?>')">-</button>
                                                        </td>
                                                    <?php else : ?>
                                                        <td class="text-center">
                                                            <button type="button" class="btn btn-sm text-center text-color-head <?= $formattedData['onTime'] ? 'colorGreen' : 'colorRed' ?> waves-effect waves-light" onclick="openModal('<?= $formattedData['platNo'] ?>')">
                                                                <?= $formattedData['onTime'] ? 'OK' : 'NG' ?>
                                                            </button>
                                                        </td>
                                                    <?php endif; ?>

                                                </tr>
                                            <?php endforeach ?>
                                        <?php else : ?>
                                            <tr>
                                                <td class="text-color-head" colspan="4">Data supplier tidak tersedia</td>
                                            </tr>
                                        <?php endif ?>
                                    </tbody>
                                </table>
                                <!-- End Table -->
                                <!-- Modal NG -->
                                <div class="modal fade" id="modalDetails" tabindex="-1" role="dialog" aria-labelledby="modalDetailsLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="modalDetailsLabel">Details</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body-ng">
                                                <!-- Content will be filled by JavaScript -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    </div>
                </div>
            </div> <!-- End Col -->
        </div> <!-- End Row -->

    </div> <!-- End Container -->
</div>
<!-- End Wrapper -->

<script>
    $(document).ready(function() {
        var table = $('#datatable').DataTable();

        $("#datatable tfoot th").each(function(i) {
            var select = $('<select><option value=""></option></select>')
                .appendTo($(this).empty())
                .on('change', function() {
                    table.column(i)
                        .search($(this).val())
                        .draw();
                });

            table.column(i).data().unique().sort().each(function(d, j) {
                select.append('<option value="' + d + '">' + d + '</option>')
            });
        });
    });
</script>

<script>
    var formattedDatasTruckJson = <?= json_encode($formattedDatasTruck); ?>;
    // console.log(formattedDatasTruckJson);

    function openModal(platNo) {
        var filteredData = yourFilterFunction(platNo); // Gantilah dengan logika Anda

        var modalBody = document.querySelector('#modalDetails' + ' .modal-body-ng');
        modalBody.innerHTML = '<table class="table table-bordered">' +
            '<tr><th class="text-center">Telat/Tidak Sesuai Jadwal</th><td class="text-center px-lg-4' + (filteredData.outOffSchedule == true ? ' colorGreen' : ' colorRed') + '"> </td></tr>' +
            '<tr><th class="text-center">Telat QC</th><td class="text-center px-lg-4' + (filteredData.onTimeQC == true ? ' colorGreen' : ' colorRed') + '"> </td></tr>' +
            '<tr><th class="text-center">Telat WHC</th><td class="text-center px-lg-4' + (filteredData.onTimeWH == true ? ' colorGreen' : ' colorRed') + '"> </td></tr>' +
            // '<tr><th class="text-center">Tidak Scan</th><td class="text-center px-lg-4' + (filteredData.notScan == false ? ' colorGreen' : ' colorRed') + '"> </td></tr>' +
            '</table>';

        $('#modalDetails').modal('show'); // Menampilkan modal
    }

    function yourFilterFunction(platNo) {
        // Ganti dengan logika filter Anda berdasarkan platNo
        // Contoh:
        var filteredData = formattedDatasTruckJson.find(function(data) {
            return data.platNo === platNo;
        });
        console.log(filteredData);
        return filteredData;
    }
</script>
<?= $this->endSection(); ?>