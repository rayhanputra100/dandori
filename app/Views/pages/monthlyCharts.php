<?= $this->extend('layouts/template/templateMonthlyCharts'); ?>

<?= $this->section('content'); ?>

<?php

// dd($averageResults);
?>
<!-- Wrapper -->
<div class="wrapper mt-3">

    <!-- Container -->
    <div class="container-fluid">
        <!-- Modal -->
        <div class="modal fade" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="dataModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title text-center" id="dataModalLabel">Data Supplier (Delay)</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <table class="bordered m-auto">
                            <thead class="bordered">
                                <th class="text-center px-3 bordered">No</th>
                                <th class="text-center px-3 bordered">Name Suppliers</th>
                                <th class="text-center px-3 bordered">Plat No</th>
                                <th class="text-center px-3 bordered">Plan In</th>
                                <th class="text-center px-3 bordered">Sec In</th>
                                <th class="text-center px-3 bordered">Total Delay</th>
                            </thead>
                            <tbody id="dataBody"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <h4 class="page-title fs-4">Dashboard Charts Incoming</h4>
                </div>
            </div>
        </div>
        <!-- End Page-Title -->

        <div class="row">
            <div class="col-12">
                <div class="card m-b-30">
                    <div class="row" style="margin: 10px 0 0 10px;">
                        <div class="col-2">
                            <form action="<?= site_url('monthly_charts') ?>" method="get">
                                <label for="filter" class="form-label mb-1">Filter</label>
                                <label for="filter" class="form-label mb-1 mt-1" style="font-size: 12px !important;">*Filter for month changes, not day.</label>
                                <input type="text" class="form-control mb-2" name="date" placeholder="<?= $date == null ? date('Y/m/d') : $filterDate = date('Y/m/d', strtotime($date)); ?>" id="mdate">
                                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-filter"></i> Filter</button>
                            </form>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row mt-3">
                            <div class="col d-flex justify-content-center">
                                <div class="border-0 p-2 rounded-4 my-bg-dark">
                                    <figure class="highcharts-figure">
                                        <div id="containerNGSup"></div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col d-flex justify-content-center">
                                <div class="border-0 p-2 rounded-4 my-bg-dark">
                                    <figure class="highcharts-figure">
                                        <div id="containerNGYear"></div>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>