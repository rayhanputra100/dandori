<?= $this->extend('layouts/template/templateScan'); ?>

<?= $this->section('content'); ?>

<!-- Wrapper -->
<div class="wrapper mt-3">

    <!-- Container -->
    <div class="container-fluid mt-5">
        <div class="row">
            <?php if (session()->getFlashdata('success')) : ?>
                <div class="col-10"></div>
                <div class="col-2">
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?= session()->getFlashdata('success', 5); ?>
                    </div>
                </div>
            <?php elseif (session()->getFlashdata('warning')) : ?>
                <div class="col-10"></div>
                <div class="col-2">
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <?= session()->getFlashdata('warning', 5); ?>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box text-center">
                    <h4 class="page-title fs-4">SCAN ANDON</h4>
                </div>
            </div>
        </div>
        <!-- End Page-Title -->

        <!-- Table -->
        <div class="row">
            <div class="col-12">
                <div class="card d-flex justify-content-center">
                    <div class="card-body text-center">
                        <h4 class="mt-0 header-title fs-5">SCAN QC </h2>
                            <form action="<?php echo base_url(); ?>scan" method="post">
                                <div class="row mb-3">
                                    <div class="col">
                                        <label for="scanDN" class="form-label mb-2 text-center">Scan Here !</label>
                                        <input type="text" class="form-control mb-3" name="noDN" id="noDN" placeholder="Nomer DN" autofocus>
                                    </div>
                                </div>
                                <button class="btn btn-md btn-primary" type="submit">Submit</button>
                            </form>
                    </div>
                </div>
            </div> <!-- End Col -->
        </div> <!-- End Row -->

    </div> <!-- End Container -->
</div>
<!-- End Wrapper -->
<?= $this->endSection(); ?>