USE [DB_SUP]
GO
	/****** Object:  Table [dbo].[T_Area]    Script Date: 6/21/2023 1:28:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO CREATE TABLE [dbo].[T_Area](
		[id] [int] IDENTITY(1, 1) NOT NULL,
		[name_supplier] [varchar](50) NULL,
		[area] [varchar](50) NULL,
		[plan_in] [varchar](50) NULL,
		CONSTRAINT [PK_T_Area] PRIMARY KEY CLUSTERED ([id] ASC) WITH (
			PAD_INDEX = OFF,
			STATISTICS_NORECOMPUTE = OFF,
			IGNORE_DUP_KEY = OFF,
			ALLOW_ROW_LOCKS = ON,
			ALLOW_PAGE_LOCKS = ON,
			OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF
		) ON [PRIMARY]
	) ON [PRIMARY]

	/* NEW TABLE !!!! */
GO
	/****** Object:  Table [dbo].[TScan]    Script Date: 6/21/2023 1:28:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO CREATE TABLE [dbo].[TScan](
		[id] [int] IDENTITY(1, 1) NOT NULL,
		[noDN] [varchar](50) NULL,
		[status] [varchar](50) NULL,
		[created_at] [datetime] NULL,
		[updated_at] [datetime] NULL,
		CONSTRAINT [PK_TScan] PRIMARY KEY CLUSTERED ([id] ASC) WITH (
			PAD_INDEX = OFF,
			STATISTICS_NORECOMPUTE = OFF,
			IGNORE_DUP_KEY = OFF,
			ALLOW_ROW_LOCKS = ON,
			ALLOW_PAGE_LOCKS = ON,
			OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF
		) ON [PRIMARY]
	) ON [PRIMARY]
GO